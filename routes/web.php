<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('events', 'WEB\EventController');

Route::resource('activities', 'WEB\ActivityController')->except([
    'create'
]);

Route::group(['middleware' => ['admin']], function () {

    Route::get('/events/organizers/search/{event_id}', 'WEB\EventController@searchOrganizers')->name('organizers.search');

    Route::get('/events/organizers/{event_id}', 'WEB\EventController@createOrganizers')->name('organizers.create');

    Route::post('/events/organizers', 'WEB\EventController@storeOrganizers')->name('organizers.store');

});

Route::group(['middleware' => ['organizer']], function () {

    Route::get('/activities/create/{event_id}', 'WEB\ActivityController@create')->name('activities.create');

    Route::get('/activities/referees/search/{activity_id}', 'WEB\ActivityController@searchReferees')->name('referees.search');

    Route::get('/activities/referees/{activity_id}', 'WEB\ActivityController@createReferees')->name('referees.create');

    Route::post('/activities/referees', 'WEB\ActivityController@storeReferees')->name('referees.store');

});