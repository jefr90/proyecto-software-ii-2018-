<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user/register', 'API\Auth\RegisterController@register');
Route::post('user/login', 'API\Auth\LoginController@login');

Route::group(['middleware' => ['jwt.auth']], function() {
    //Usuario
    Route::get('users', 'API\UserController@index');
    Route::get('user/{user}', 'API\UserController@show');
    Route::get('user/auth/data', 'API\UserController@userAuth');
    //Actividades
    Route::get('activities','API\ActivityController@index');
    Route::get('activities/{activity}', 'API\ActivityController@show');
    Route::get('activities/{activity}/referees', 'API\ActivityController@referees');
    Route::get('activities/user/auth', 'API\ActivityController@userAuth');
    Route::get('activities/user/{user}', 'API\ActivityController@showUser');
    //Eventos
    Route::get('events', 'API\EventController@index');
    Route::get('events/{event}', 'API\EventController@show');
    //Lugares
    Route::get('places', 'API\PlaceController@index');
    Route::get('places/{place}', 'API\PlaceController@show');
});