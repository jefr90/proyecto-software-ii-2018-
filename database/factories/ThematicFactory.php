<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Thematic::class, function (Faker $faker) {
    return [
        'description' => $faker->title
    ];
});
