<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Event::class, function (Faker $faker) {
    return [
        'description' => $faker->title,
        'start_date' => Carbon\Carbon::now(),
        'end_date' => '2018-10-25 08:37:17'
    ];
});
