<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Necessity::class, function (Faker $faker) {
    $activities=App\Models\Activity::all();
    return [
        'description'=>$faker->name,
        'activity_id'=>$activities->random()
    ];
});
