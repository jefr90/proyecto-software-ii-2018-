<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Activity::class, function (Faker $faker) {
    $events=App\Models\Event::all();
    $users=App\Models\User::all();
    $thematics=App\Models\Thematic::all();
    $places=App\Models\Place::all();
    return [
        'description' => $faker->name,
        'type'=>'Mega Actividad',
        'start_date'=>'2018-10-15 08:37:17',
        'end_date'=>'2018-10-25 06:37:17',
        'status'=>1,
        'capacity' => 100,
        'event_id'=> $events->random(),
        'user_id'=>$users->random(),
        'place_id'=>$places->random(),
        'thematic_id'=>$thematics->random(),
        'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
    ];
});
