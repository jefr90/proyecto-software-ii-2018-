<?php

use Illuminate\Database\Seeder;

class ThematicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Thematic::class,10)->create();
    }
}
