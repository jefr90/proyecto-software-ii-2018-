<?php

use Illuminate\Database\Seeder;

class NecessitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Necessity::class,20)->create();
    }
}
