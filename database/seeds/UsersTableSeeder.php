<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        [
          'name' => 'Administrador',
          'email' => 'admin@admin.com',
          'password' => Hash::make("admin"),
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'name' => 'Daniel',
          'email' => 'daniel@gmail.com',
          'password' => Hash::make("123456"),
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'name' => 'Jesus',
          'email' => 'jesus@gmail.com',
          'password' => Hash::make("123456"),
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'name' => 'Yldemaro',
          'email' => 'yldemaro@gmail.com',
          'password' => Hash::make("123456"),
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'name' => 'Eder',
          'email' => 'eder@gmail.com',
          'password' => Hash::make("123456"),
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
      ]);

      $admin_role = Role::where('name','admin')->first();
      $super_admins = User::all();
      
      foreach($super_admins as $admin){
        $admin->roles()->attach($admin_role);
      }
      
      $roles = Role::where('id','>',1)->get();

      factory(User::class, 20)->create()->each(function ($u) use ($roles) {
        $u->roles()->attach($roles->random());
      });
    }
  }
