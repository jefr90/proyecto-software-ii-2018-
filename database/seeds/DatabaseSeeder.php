<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PlacesTableSeeder::class,
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            EventsTableSeeder::class,
            ThematicsTableSeeder::class,
            ActivitiesTableSeeder::class,
            NecessitiesTableSeeder::class
        ]);
    }
}
