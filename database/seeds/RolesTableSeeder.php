<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([  
            ['name' => 'admin','display_name' => 'Administrador'],
            ['name' => 'organizer','display_name' => 'Organizador'],
            ['name' => 'referee','display_name' => 'Arbitro'],
            ['name' => 'responsible','display_name' => 'Responsable']
        ]);
    }
}
