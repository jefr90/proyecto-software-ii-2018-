<?php

use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=App\Models\User::all();
        $data = collect();
        foreach($users as $u){
            $referee = $u->roles()->where('name','referee')->first();
            if(!empty($referee)) {$data->push($referee);}
        }
        factory(App\Models\Activity::class,20)->create()->each(function ($a) use ($data) {
            $referee = $data->pluck('pivot.user_id')->random();
            $a->referees()->attach($referee);
            $a->evaluations()->attach($referee, ['evaluation'=>rand(1,10)]);
        });
    }
}
