<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('places')->insert([
        [
          'description' => 'Biblioteca',
          'capacity' => 50,
          'location' => 'Modulo 2, PB',
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'description' => 'Salon de usos multiples',
          'capacity' => 100,
          'location' => 'Modulo Informatica, PB, frente al cafetin',
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'description' => 'Salones de clases informatica',
          'capacity' => 40,
          'location' => 'Modulo 1, PB y Piso 1',
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
          'description' => 'Salones de clases de otras carreras',
          'capacity' => 40,
          'location' => 'Modulo 2, Piso 1',
          'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ],
      ]);
    }
}
