<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $data = collect();
        foreach($users as $u){
            $organizer = $u->roles()->where('name','organizer')->first();
            if(!empty($organizer)) {$data->push($organizer);}
        }
        factory(Event::class,5)->create()->each(function ($e) use ($data) {
            $e->organizers()->attach($data->pluck('pivot.user_id')->random());
        });
    }
}
