<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTrait
{
    public function apiException($request, $e)
    {
        if ($e instanceof ModelNotFoundException)
        {
            return $this->errorResponse('Modelo no encontrado');
        }

        if ($e instanceof NotFoundHttpException)
        {
            return $this->errorResponse('Ruta incorrecta');
        }

        return parent::render($request, $e);
    }

    protected function errorResponse($message = '')
    {
        return response()->json(['status'=>false, 'message'=>$message, 'data'=>null], 200);
    }
}

