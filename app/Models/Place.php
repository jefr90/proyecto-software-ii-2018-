<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }
}
