<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Necessity extends Model
{
    public function activity()
    {
        return $this->belongsTo('App\Models\Activity');
    }
}
