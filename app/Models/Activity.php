<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    protected $fillable = [
        'description', 'start_date', 'end_date','user_id', 'type', 'place_id','capacity','status','event_id','thematic_id',
    ];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place');
    }

    public function thematic()
    {
        return $this->belongsTo('App\Models\Thematic');
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function necessities()
    {
        return $this->hasMany('App\Models\Necessity');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\File');
    }

    public function evaluations()
    {
        return $this->belongsToMany('App\Models\User', 'evaluation_presentation');
    }

    public function referees()
    {
        return $this->belongsToMany('App\Models\User', 'presentation_referee');
    }
}
