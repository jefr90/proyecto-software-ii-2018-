<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thematic extends Model
{
    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }
}
