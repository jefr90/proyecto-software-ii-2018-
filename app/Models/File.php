<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function activity()
    {
        return $this->belongsTo('App\Models\Activity');
    }
}
