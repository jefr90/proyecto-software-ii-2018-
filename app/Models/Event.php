<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

	protected $fillable = [
        'description', 'start_date', 'end_date',
    ];


    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }

    public function organizers()
    {
        return $this->belongsToMany('App\Models\User', 'event_organizer');
    }

}
