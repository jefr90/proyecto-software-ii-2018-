<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Users as UserResource;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        return response()->json([
            'status' => true,
            'message' => 'Todos los usuarios',
            'data' => UserResource::collection(User::all())
        ],200);
    }

    public function show(User $user){
        return response()->json([
            'status' => true,
            'message' => 'Usuario encontrado',
            'data' => new UserResource($user)
        ],200);
    }

    public function userAuth(){
        return response()->json([
            'status' => true,
            'message' => 'Usuario autenticado',
            'data' => new UserResource(\Auth::user())
        ],200);
    }
}
