<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use JWTFactory;
use JWTAuth;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => false,
                    'message' => 'Credenciales invalidas',
                    'data' => null
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => false,
                'message' => 'Token no se puede crear',
                'data' => null
            ], 500);
        }
        
        $user = User::where('email', $request->email)->with('roles')->first();
        $roles = array_get($user, 'roles');

        foreach($roles as $role){
            if(array_get($role, 'name') == 'admin' || array_get($role, 'name') == 'organizer'){ 
                return response()->json([
                    'status' => true,
                    'message' => 'Usuario Logueado',
                    'data' => $token
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Usuario no permitido | Acceso negado',
                    'data' => null
                ]);
            }
        }
    }
}
