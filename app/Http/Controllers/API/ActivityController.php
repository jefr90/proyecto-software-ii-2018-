<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Resources\Activities as ActivitiesResource;
use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\User;
use Auth;

class ActivityController extends Controller
{
    public function index () {
        return response()->json([
            'status' => true,
            'message' => 'Todas las actividades',
            'data' => ActivitiesResource::collection(Activity::all())
        ],200);
    }

    public function show(Activity $activity){
        return response()->json([
            'status' => true,
            'message' => 'Actividad encontrada',
            'data' => new ActivitiesResource($activity)
        ],200);
    }

    public function userAuth () {
        return response()->json([
            'status' => true,
            'message' => 'Actividades del usuario en sesion',
            'data' => ActivitiesResource::collection(Auth::user()->activities)
        ],200);
    }

    public function showUser (User $user) {
        return response()->json([
            'status' => true,
            'message' => 'Actividades del usuario buscado',
            'data' => ActivitiesResource::collection($user->activities)
        ],200);
    }

    public function referees (Activity $activity) {
        return response()->json([
            'status' => true,
            'message' => 'Referees de la actividad',
            'data' => $activity->referees
        ],200);
    }
}
