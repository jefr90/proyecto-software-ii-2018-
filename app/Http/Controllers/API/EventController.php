<?php

namespace App\Http\Controllers\API;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Events as EventsResource;
use App\Models\User;
use App\Models\Event;

class EventController extends Controller
{
    public function index(){
        return response()->json([
            'status'=>true,
            'message'=>'Todos los eventos registrados',
            'data'=> EventsResource::collection(Event::all())
        ],200);
    }

    public function show(Event $event){
        return response()->json([
            'status' => true,
            'message' => 'Evento encontrado',
            'data' => new EventsResource($event)
        ],200);
    }
}
