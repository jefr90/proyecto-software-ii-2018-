<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Thematic;

class ThematicController extends Controller
{
    public function index(){
        return response()->json([
            'status'=>true,
            'message'=>'Todos los temas registrados',
            'data'=> Thematic::all()
        ],200);
    }
}
