<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Places as PlaceResource;
use App\Models\User;
use App\Models\Place;

class PlaceController extends Controller
{
    public function index(){
        return response()->json([
            'status'=>true,
            'message'=>'Todos los lugares registrados',
            'data'=> PlaceResource::collection(Place::all())
        ],200);
    }

    public function show(Place $place){
        return response()->json([
            'status' => true,
            'message' => 'Lugar encontrado',
            'data' => new PlaceResource($place)
        ],200);
    }
}
