<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Session;

class EventController extends Controller
{
    public function index()
    {
        $events = Event::all();

        return view('events.index')->with('events', $events);
    }

    public function show($event)
    {
        $event = Event::find($event);

        return view('events.show')->with('event', $event);
    }

    public function create()
    {
        return view('events.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'description' => 'required|unique:events|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
        ]);

        $event = Event::create($validatedData);

        $request->session()->flash('status', 'Evento creado exitosamente');

        return view('events.show')->with('event', $event);
    }

    
    public function searchOrganizers($event_id)
    {
        $event = Event::findOrFail($event_id);

        return view('organizers.search')->with('event', $event);
    }

    public function createOrganizers(Request $request)
    {
        $users = User::where('name', 'like', '%'.$request->search.'%')
                      ->orWhere('email', 'like', '%'.$request->search.'%')
						 ->orWhere('phone', 'like', '%'.$request->search.'%')->get();
    
        $event = Event::find($request->event_id);
        
        $admins = Role::where('name', 'admin')->first()->users;
        
        $organizers = $event->organizers;
        
        $new_organizers = $users->diff($organizers->merge($admins))->take(10);
        
        if ($new_organizers->count() < 1) {
            $request->session()->flash('info', 'No se encontraron resultados de búsqueda. Por favor, intente con otro término');
            return back();
        }
        return view('organizers.create')->with(['users' => $new_organizers, 'event' => $event]);
    }

    public function storeOrganizers(Request $request)
    {
        $event = Event::findOrFail($request->event_id);

        $event->organizers()->attach($request->organizers);

        $request->session()->flash('status', 'Organizador(es) añadidos exitosamente');

        return redirect()->route('events.show', $event->id);
    }
}
