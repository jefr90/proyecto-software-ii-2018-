<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Event;
use App\Models\User;
use App\Models\Place;
use App\Models\Thematic;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Auth;

class ActivityController extends Controller
{
    public function show($activity_id)
    {
        $activity = Activity::findOrFail($activity_id);

        return view('activities.show')->with('activity', $activity);
    }

    public function create($event_id)
    {
        $event = Event::findOrFail($event_id);

        $users = User::orderBy('name')->get();

        $places = Place::all();

        $thematics = Thematic::all();

        return view('activities.create')->with([
            'event' => $event,
            'users' => $users,
            'places' => $places,
            'thematics' => $thematics
        ]);
    }

    public function store(Request $request)
    {
        $request->start_date = new Carbon($request->start_date);
        $request->end_date = new Carbon($request->end_date);

        $validatedData = $request->validate([
            'description' => 'required|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'user_id' => 'required|exists:users,id',
            'event_id' => 'required|exists:events,id',
            'type' => [
                'required',
                Rule::in(['Forum', 'Presentation', 'Conference']),
            ],
            'place_id' => 'required|exists:places,id',
            'capacity' => 'required|numeric',
            'status' => [
                'required',
                Rule::in([0]),
            ],
            'thematic_id' => 'required|exists:thematics,id',
        ]);

        $colliding_date =	Activity::where('event_id', $request->event_id)
                            ->where('place_id', $request->place_id)
                            ->whereBetween('start_date', [$request->start_date, $request->end_date])
                            ->whereBetween('end_date', [$request->start_date, $request->end_date])
                            ->get();

        if ($colliding_date->count() > 0) {
            $request->session()->flash('error', 'No se pudo crear la actividad. Ya existe una actividad en el horario y espacio indicados.');
            return back()->withInput();
        }
        $colliding_conference =	Activity::where('event_id', $request->event_id)
                                ->whereBetween('start_date', [$request->start_date, $request->end_date])
                                ->whereBetween('end_date', [$request->start_date, $request->end_date])
                                -where('type', 'Conference')
                                ->get();

        if ($colliding_conference->count() > 0) {
            $request->session()->flash('error', 'No se pudo crear la actividad. Existe una Conferencia en el horario indicado.');
            return back()->withInput();
        }

        $activity = Activity::create($validatedData);

        return view('activities.show')->with('activity', $activity);
    }

    public function searchReferees($activity_id)
    {
        $activity = Activity::findOrFail($activity_id);

        return view('referees.search')->with('activity', $activity);
    }
    
    public function createReferees(Request $request)
    {
        $users = User::where('name', 'like', '%'.$request->search.'%')
                      ->orWhere('email', 'like', '%'.$request->search.'%')
                         ->orWhere('phone', 'like', '%'.$request->search.'%')->get();
                         
        $activity = Activity::find($request->activity_id);
         
        $referees = $activity->referees;
        
        $new_referees = $users->diff($referees);

        $new_referees = $new_referees->where('id', '!=', Auth::user()->id);
    
        if ($new_referees->count() < 1) {
            $request->session()->flash('info', 'No se encontraron resultados de búsqueda. Por favor, intente con otro término');
            return back();
        }
        return view('referees.create')->with(['users' => $new_referees->take(10), 'activity' => $activity]);
    }
    
    public function storeReferees(Request $request)
    {
        $activity = Activity::findOrFail($request->activity_id);

        if ($activity->type !='Presentation') {
            return back();
        }

        $activity->referees()->attach($request->referees);

        $request->session()->flash('status', 'Árbitros añadidos exitosamente');

        return redirect()->route('activities.show', $activity->id);
    }
}
