<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Activities extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'description' => $this->description,
            'type' => $this->type,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'capacity' => $this->capacity,
            'status' => $this->status,
            'event' => $this->event,
            'responsible' => $this->responsible,
            'place' => $this->place,
            'thematic' => $this->thematic,
            'necessities' => $this->necessities,
            'evaluations' => $this->evaluations,
            'referees' => $this->referees,
            'documents' => $this->documents,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
    }
}
