<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Event;
use App\Models\Activity;

class CheckOrganizer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('event_id')) {
            $event = Event::findOrFail($request->route('event_id'));
            if (Auth::user() && (Auth::user()->roles()->where('name', 'admin')->first() || $event->organizers->contains(Auth::user()->id))) {
                return $next($request);
            }
        } elseif ($request->route('activity_id')) {
            $activity = Activity::findOrFail($request->route('activity_id'));
            if (Auth::user() && (Auth::user()->roles()->where('name', 'admin')->first() || $activity->event->organizers->contains(Auth::user()->id))) {
                return $next($request);
            }
        } elseif ($request->activity_id) {
            $activity = Activity::findOrFail($request->activity_id);
            if (Auth::user() && (Auth::user()->roles()->where('name', 'admin')->first() || $activity->event->organizers->contains(Auth::user()->id))) {
                return $next($request);
            }
        } else {
            return redirect()->back();
        }
    }
}
