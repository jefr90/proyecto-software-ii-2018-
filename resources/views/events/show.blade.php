@extends('layouts.app')

@section('content')
<div class="container">

  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div class="mb-3">
    <h3 class="d-inline">
      {{$event->description}}
    </h3>
    @if(Auth::user() && Auth::user()->roles()->where('name','admin')->first()) 
     <a class="btn btn-info mb-3 float-right mr-2" href="{{ route('organizers.search', $event->id) }}">Añadir organizadores</a>
     <a class="btn btn-success mb-3 float-right mr-2" href="{{ route('activities.create', $event->id) }}">Crear actividad</a>
    @endif
  </div>
  <div class="bs-component">
    <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">Descripción</th>
          <th scope="col">Tipo</th>
          <th scope="col">Capacidad</th>
          <th scope="col">Espacio</th>
          <th scope="col">Temática</th>
          <th scope="col">Hora Inicio</th>
          <th scope="col">Hora Fin</th>

        </tr>
      </thead>
      <tbody>
        @foreach($event->activities as $activity)
        <tr class="clickable-row" data-href="{{ route('activities.show', $activity->id) }}">
         <td>
           {{ $activity->description }}
         </td>
         <td>
          {{ __($activity->type) }}
        </td>
        <td>
          {{ $activity->capacity }}
        </td>
        <td>
          {{ $activity->place->description }}
        </td>
        <td>
          {{ $activity->thematic->description }}
        </td>
        <td>
          {{ Carbon\Carbon::parse($activity->start_date)->format('d/m/Y h:m a') }}
        </td>
        <td>
          {{ Carbon\Carbon::parse($activity->end_date)->format('d/m/Y h:m a') }}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@if($event->organizers->count() > 0)
<div class="mb-3">
  <h4 class="d-inline">
    Organizadores
  </h4>
</div>
<div class="bs-component">
  <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">E-mail</th>
        <th scope="col">Telefono</th>

      </tr>
    </thead>
    <tbody>
      @foreach($event->organizers as $organizer)
      <tr>
       <td>
         {{ $organizer->name }}
       </td>
       <td>
        {{ $organizer->email }}
      </td>
      <td>
        {{ $organizer->phone }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>


@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
