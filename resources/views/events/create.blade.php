@extends('layouts.app')

@section('content')
<div class="container">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  @if ($errors->any())
  <div class="alert alert-danger" role="alert">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
</div>
<div class="container d-flex justify-content-center">
  <div class="card border-primary col-lg-6">
    <div class="card-header">Crear evento</div>
    <div class="card-body">
      <h4 class="card-title">Crear un evento</h4>
      <form method="POST" action="{{ route('events.store') }}">
        @csrf
        <div class="form-group">
          <label for="inputDescription">Descripción</label>
          <input type="text" class="form-control" id="inputDescription" name="description" aria-describedby="descriptionHelp" placeholder="Ingrese la descripción" required>
          <small id="descriptionHelp" class="form-text text-muted">Ingrese la descripción del evento a crear</small>
        </div>
        <div class="form-group">
          <label for="inputStartDate">Fecha de inicio</label>
          <input type="datetime-local" class="form-control" id="inputStartDate" name="start_date" aria-describedby="start_dateHelp" placeholder="DD/MM/AAAA" required>
          <small id="start_dateHelp" class="form-text text-muted">Ingrese la fecha de inicio del evento a crear</small>
        </div>
        <div class="form-group">
          <label for="inputEndDate">Fecha de fin</label>
          <input type="datetime-local" class="form-control" id="inputEndDate" name="end_date" aria-describedby="end_dateHelp" placeholder="DD/MM/AAAA" required>
          <small id="end_dateHelp" class="form-text text-muted">Ingrese la fecha de fin del evento a crear</small>
        </div>
        <button type="submit" class="btn btn-primary">Crear evento</button>
      </form>
    </div>
  </div>
</div>


@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
