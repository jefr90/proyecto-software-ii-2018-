@extends('layouts.app')

@section('content')
<div class="container">

  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif

  <div class="mb-3">
    <h1 class="d-inline"> Eventos </h1> 
    @if(Auth::user() && Auth::user()->roles()->where('name','admin')->first()) 
    <a class="btn btn-success mb-3 float-right" href="{{ route('events.create') }}">Crear evento</a>
    @endif
  </div>    
  <div class="bs-component">
    <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">Descripción</th>
          <th scope="col">Fecha Inicio</th>
          <th scope="col">Fecha Fin</th>
        </tr>
      </thead>
      <tbody>
        @foreach($events as $event)
        <tr class="clickable-row" data-href="{{ route('events.show', $event->id) }}">
         <td>
          {{$event->description}}
        </td>
        <td>

         {{Carbon\Carbon::parse($event->start_date)->format('d/m/Y')}}
       </td>
       <td>
         {{Carbon\Carbon::parse($event->end_date)->format('d/m/Y')}}
       </td>
     </tr>

     @endforeach
   </tbody>
 </table>
</div>
</div>


@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
