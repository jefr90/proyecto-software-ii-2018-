@extends('layouts.app')

@section('content')
<div class="container">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  @if (session('info'))
  <div class="alert alert-info" role="alert">
    {{ session('info') }}
  </div>
  @endif
  @if ($errors->any())
  <div class="alert alert-danger" role="alert">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
</div>
<div class="container d-flex justify-content-center">
  <div class="card border-primary col-lg-6">
    <div class="card-header">Añadir organizador</div>
    <div class="card-body">
      <h4 class="card-title">Búsqueda de usuarios</h4>
      <form method="GET" action="{{ route('organizers.create', $event->id) }}">
        <div class="form-group">
          <label for="inputSearch">Término de búsqueda</label>
          <input type="text" class="form-control" id="inputSearch" name="search" aria-describedby="searchHelp" placeholder="Nombre o e-mail del usuario a buscar" required>
          <small id="descriptionHelp" class="form-text text-muted">Ingrese el nombre o e-mail del usuario a buscar</small>
        </div>
        <button type="submit" class="btn btn-primary">Buscar</button>
      </form>
    </div>
  </div>
</div>


@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
