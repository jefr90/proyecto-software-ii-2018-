@extends('layouts.app')

@section('content')
<div class="container">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  @if ($errors->any())
  <div class="alert alert-danger" role="alert">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
</div>
<div class="container d-flex justify-content-center">
  <div class="card border-primary col-lg-8">
    <div class="card-header">Añadir organizador</div>
    <div class="card-body">
      <h4 class="card-title">Búsqueda de usuarios</h4>
      <form method="GET" action="{{ route('organizers.create', $event->id) }}">
        <div class="form-group">
          <label for="inputSearch">Término de búsqueda</label>
          <input type="text" class="form-control" id="inputSearch" name="search" aria-describedby="searchHelp" placeholder="Nombre o e-mail del usuario a buscar" required>
          <small id="searchHelp" class="form-text text-muted">Ingrese el nombre o e-mail del usuario a buscar</small>
        </div>
        <button type="submit" class="btn btn-primary">Buscar</button>
      </form>
    </div>
  </div>
  </div>
  <div class="container d-flex justify-content-center">
<div class="card border-primary col-lg-8 mt-3">
<div class="card-header">Seleccione organizadores a añadir</div>
<div class="card-body">
<form method="POST" action="{{ route('organizers.store')}}">
@csrf
<input type="hidden" name="event_id" value="{{$event->id}}">
    <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">Correo</th>
          <th scope="col">Seleccionar</th>
        </tr>
      </thead>
      <tbody>
      @foreach($users as $user)
        <tr>
         <td>
          {{ $user->name }}
        </td>
        <td>
         {{ $user->email }}
       </td>
       <td class="text-center">
            <input type="checkbox" name="organizers[]" value="{{$user->id}}">
       </td>
     </tr>

     @endforeach
   </tbody>
 </table>
 <button type="submit" class="btn btn-info">Añadir organizadores</button>
 </form>
</div>
</div>
</div>
</div>

@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
