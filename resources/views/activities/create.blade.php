@extends('layouts.app')

@section('content')
<div class="container">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
    @if (session('error'))
  <div class="alert alert-danger" role="alert">
    {{ session('error') }}
  </div>
  @endif
  @if ($errors->any())
  <div class="alert alert-danger" role="alert">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
</div>
<div class="container d-flex justify-content-center">
  <div class="card border-primary col-lg-6">
    <div class="card-header">Crear actividad</div>
    <div class="card-body">
      <h4 class="card-title">Crear una actividad</h4>
      <form method="POST" action="{{ route('activities.store') }}">
        @csrf
        <input type="hidden" name="event_id" value="{{$event->id}}">
        <input type="hidden" name="status" value="0">
        <div class="form-group">
          <label for="inputDescription">Descripción</label>
          <input type="text" class="form-control" id="inputDescription" name="description" aria-describedby="descriptionHelp" placeholder="Ingrese la descripción" value="{{ old('description') }}" required>
          <small id="descriptionHelp" class="form-text text-muted">Ingrese la descripción de la actividad a crear</small>
        </div>
        <div class="form-group">
          <label for="inputStartDate">Fecha de inicio</label>
          <input type="datetime-local" class="form-control" id="inputStartDate" name="start_date" aria-describedby="start_dateHelp" placeholder="DD/MM/AAAA" value="{{ old('start_date') }}" required>
          <small id="start_dateHelp" class="form-text text-muted">Ingrese la fecha de inicio de la actividad a crear</small>
        </div>
        <div class="form-group">
          <label for="inputEndDate">Fecha de fin</label>
          <input type="datetime-local" class="form-control" id="inputEndDate" name="end_date" aria-describedby="end_dateHelp" placeholder="DD/MM/AAAA" value="{{ old('end_date') }}" required>
          <small id="end_dateHelp" class="form-text text-muted">Ingrese la fecha de fin de la actividad a crear</small>
        </div>

        <div class="form-group">
          <label for="inputCapacity">Capacidad de la actividad</label>
          <input type="number" class="form-control" id="inputCapacity" name="capacity" aria-describedby="capacityHelp" placeholder="Capacidad" value="{{ old('capacity') }}" required>
          <small id="capacityHelp" class="form-text text-muted">Ingrese la capacidad de la actividad a crear</small>
        </div>

          <div class="form-group">
                    <label for="selectResponsible">Responsable de la actividad</label>
                    <select class="form-control" name="user_id" id="selectResponsible">
                      <option value="" selected>Seleccionar responsable</option>
                      @foreach($users as $user)
                      <option value="{{ $user->id }}">{{ $user->name }}</option>
                      @endforeach
                    </select>
         </div>
        <div class="form-group">
                    <label for="selectType">Tipo de actividad</label>
                    <select class="form-control" name="type" id="selectType">
                      <option value="" selected>Seleccionar tipo</option>
                      <option value="Forum">Foro</option>
                      <option value="Presentation">Ponencia</option>
                      <option value="Conference">Conferencia</option>
                    </select>
         </div>

         <div class="form-group">
                    <label for="selectPlace">Espacio donde se dará</label>
                    <select class="form-control" name="place_id" id="selectPlace">
                      <option value="" selected>Seleccionar espacio</option>
                      @foreach($places as $place)
                      <option value="{{ $place->id }}">{{ $place->description }}</option>
                      @endforeach
                    </select>
         </div>
         <div class="form-group">
                    <label for="selectThematic">Temática de la actividad (opcional)</label>
                    <select class="form-control" name="thematic_id" id="selectThematic">
                      @foreach($thematics as $thematic)
                      <option value="{{ $thematic->id }}">{{ $thematic->description }}</option>
                      @endforeach
                    </select>
         </div>
        <button type="submit" class="btn btn-primary">Crear actividad</button>
      </form>
    </div>
  </div>
</div>


@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
