@extends('layouts.app')

@section('content')
<div class="container">

  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <div class="mb-3">
    <h3 class="d-inline">
      Datos de actividad
    </h3>
    @if(Auth::user() && (Auth::user()->roles()->where('name','admin')->first() || $activity->event->organizers->contains(Auth::user()->id)) && $activity->type == 'Presentation')
     <a class="btn btn-info mb-3 float-right mr-2" href="{{ route('referees.search', $activity->id) }}">Añadir árbitros</a>
    @endif
  </div>
  <div class="card mb-3">
    <div class="float-left">
      <h3 class="card-header">{{ $activity->description }}</h3>
      <div class="card-body">
        <h5 class="card-title">Evento : {{ $activity->event->description }}</h5>
        <h5 class="card-title">{{ __($activity->type) }}</h5>
        <h5 class="card-title">Responsable : {{ $activity->responsible->name }}</h5>
        <h6 class="card-subtitle text-muted">Espacio : {{ $activity->place->description }}</h6>
      </div>
    </div>
    <div class="card-body">
      @if($activity->thematic)
      <p class="card-text">Temática: {{ $activity->thematic->description }}</p>

      @endif
      <p class="card-text">Capacidad: {{ $activity->capacity }} </p>
      <p class="card-text">Hora inicio:  {{ Carbon\Carbon::parse($activity->start_date)->format('d/m/Y h:m a') }} </p>
      <p class="card-text">Hora fin: {{ Carbon\Carbon::parse($activity->end_date)->format('d/m/Y h:m a') }} </p>
      <p class="card-text">Árbitros asignados: {{ $activity->referees->count() }} </p>
    </div>
    <div class="card-footer text-muted">
      Texto
    </div>
  </div>

  @if(Auth::user() && (Auth::user()->roles()->where('name','admin')->first() || $activity->event->organizers->contains(Auth::user()->id)) && $activity->type == 'Presentation')
  @if($activity->referees->count() > 0)
<div class="mb-3">
  <h4 class="d-inline">
    Árbitros
  </h4>
</div>
<div class="bs-component">
  <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">E-mail</th>
        <th scope="col">Telefono</th>

      </tr>
    </thead>
    <tbody>
      @foreach($activity->referees as $referee)
      <tr>
       <td>
         {{ $referee->name }}
       </td>
       <td>
        {{ $referee->email }}
      </td>
      <td>
        {{ $referee->phone }}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
@endif


</div>


@section('pagescript')
<script src="{{asset('/js/events.js')}}"></script>
@stop

@endsection
